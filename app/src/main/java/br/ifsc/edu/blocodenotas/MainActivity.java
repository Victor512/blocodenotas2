package br.ifsc.edu.blocodenotas;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText2);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putString("nota", editText.getText().toString());
                editor.commit();
            }
        });
        //Configuração do shared preferences
        sharedPreferences = getSharedPreferences("MinhasNotasDB",MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    @Override
    protected void onStart() {
        super.onStart();

        editText.setText(sharedPreferences.getString("nota",""));
    }
}
